
Some useful scripts for manipulating a Tryton system
through the Proteus RPC API client.

Most configuration settings are at the top of the scripts.

ecb-rates-to-base.py
 * Convert the CSV rate file from one denominator to another
   denominator.  E.g. convert the ECB (EUR) rates to CHF or GBP base.

load-exchange-rates.py
 * Loads the exchange rates from a CSV file.  It can read the
   ECB rate file or a file created by ecb-rates-to-base.py

load-invoices.py
 * Loads the static list of vendor/product/tax mappings vendors.csv
 * Creates one-line invoices based on invoices.json
