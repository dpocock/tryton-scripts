#!/usr/bin/python3

import csv
import datetime
import decimal
import getpass
import os

from proteus import config, Model, Wizard, Report

earliest_date = datetime.date.fromisoformat("2021-01-01")
base_currency = "CHF"
#input_file = "ecb-rates/eurofxref-hist.csv"
input_file = "ecb-rates/%s-fxref-hist.csv" % base_currency

decimal.getcontext().prec = 28
q = decimal.Decimal('0.000001')  # FIXME - for different currencies

def get_price(price_str):
    return decimal.getcontext().create_decimal(price_str).quantize(q)

password = getpass.getpass()

tryton_host = os.getenv("TRYTON_HOST")

config = config.set_xmlrpc("http://admin:%s@%s/tryton/" % (password, tryton_host))

Party = Model.get('party.party')
p1, = Party.find([('id', '=', '1')])
print(p1.name)

csv_currencies = []
csv_rates = {}
with open(input_file, "rt") as f:
    csv_reader = csv.reader(f, delimiter=",")
    for row in csv_reader:
        if row[0] == "Date":
            # header row
            for code in row[1:]:
                if len(code) > 0:
                    csv_currencies.append(code)
                    csv_rates[code] = {}
        else:
            row_date = datetime.date.fromisoformat(row[0])
            if row_date < earliest_date:
                continue
            i = 1
            for code in csv_currencies:
                val = row[i]
                if len(val) > 0 and val != "N/A":
                    csv_rates[code][row_date] = get_price(val)
                i = i + 1

Currency = Model.get('currency.currency')
CurrencyRate = Model.get('currency.currency.rate')

# ignore values from the CSV file if Tryton
# already has a value for the same currency and date
tryton_currencies = []
for ccode in csv_currencies:
    if ccode == base_currency:
        continue
    print("Checking existing rates for %s" % (ccode,))
    currencies = Currency.find(['code','=',ccode])
    if len(currencies) == 0:
        print("Tryton doesn't have currency: %s" % (ccode,))
    else:
        currency = currencies[0]
        tryton_currencies.append(ccode)
        rates = CurrencyRate.find([('currency','=',currency.id)])
        for rate in rates:
            if rate.date in csv_rates[ccode].keys():
                print("%s local %f tryton %f" % (rate.date, csv_rates[ccode][rate.date], rate.rate))
                del csv_rates[ccode][rate.date]

# store the rates in Tryton
for ccode in tryton_currencies:
    if ccode == base_currency:
        continue
    print("Adding rates for %s" % (ccode,))
    currencies = Currency.find(['code','=',ccode])
    if len(currencies) == 0:
        print("Tryton doesn't have currency: %s" % (ccode,))
    else:
        currency = currencies[0]
        for cdate, val in csv_rates[ccode].items():
            rate = CurrencyRate()
            rate.currency = currency
            rate.date = cdate
            rate.rate = val
            rate.save()
