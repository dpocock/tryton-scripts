#!/usr/bin/python3

import csv
import datetime
import decimal
import sys

# The purpose of this script is to read the EUR-denominated
# FX rates from the ECB file and convert them to some other
# base currency.
# The denominator doesn't have to be EUR, it is configurable.
#
# Configuration:

input_file = "ecb-rates/eurofxref-hist.csv"

# rows before this date will not be copied to the output file
earliest_date = datetime.date.fromisoformat("2021-01-01")

base_currency = sys.argv[1]

denominator_currency = "EUR"

output_file = "ecb-rates/%s-fxref-hist.csv" % base_currency

decimal.getcontext().prec = 28
q = decimal.Decimal('0.000001')  # FIXME - for different currencies

#
# Program begins here
#

csv_currencies = []
csv_rates = {}

output_rates = {}

def add_currency(code):
    if code in csv_currencies:
        print("currency %s already exists in earlier column" % code, file=sys.stderr)
        sys.exit(1)
    csv_currencies.append(code)
    csv_rates[code] = {}

def get_price(price_str):
    return decimal.getcontext().create_decimal(price_str).quantize(q)

with open(input_file, "rt") as f:
    csv_reader = csv.reader(f, delimiter=",")
    for row in csv_reader:
        if row[0] == "Date":
            # header row
            for code in row[1:]:
                if len(code) == 3:
                    add_currency(code)
            add_currency(denominator_currency)
        else:
            row_date = datetime.date.fromisoformat(row[0])
            if row_date < earliest_date:
                continue
            i = 1
            _csv_rates = {}
            for code in csv_currencies:
                val = row[i]
                if len(val) > 0 and val != "N/A":
                    _csv_rates[code] = get_price(val)
                else:
                    _csv_rates[code] = None
                i = i + 1
            # convert rates from denominator to local denominator
            if _csv_rates[base_currency] is not None:
                output_rates[row_date] = []
                den_over_base = _csv_rates[base_currency]
                _csv_rates[denominator_currency] = get_price(1.0)
                for code in csv_currencies:
                    if _csv_rates[code] is not None:
                        csv_rates[code][row_date] = (_csv_rates[code] / den_over_base).quantize(q)
                    else:
                        csv_rates[code][row_date] = None
                    output_rates[row_date].append(str(csv_rates[code][row_date]))

with open(output_file, "wt") as f:
    header_row = ["Date"] + csv_currencies
    f.write(",".join(header_row) + "\n")
    for row_date in output_rates.keys():
        row = [row_date.isoformat()] + output_rates[row_date]
        s = ",".join(row).replace("None", "N/A")
        f.write(s + "\n")

