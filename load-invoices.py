#!/usr/bin/python3

import csv
import datetime
import decimal
import getpass
import json
import os
import sys

from proteus import config, Model, Wizard, Report

decimal.getcontext().prec = 28
dContext = decimal.Context(prec=28)
q = decimal.Decimal('0.01')  # FIXME - for different currencies

def get_price(price_float):
    return dContext.create_decimal_from_float(price_float).quantize(q)

# FIXME
# - check that amount_ex + tax = amount of created invoice (rounding discrepancies)
# - post
# - set Accounting Date value?

password = getpass.getpass()

tryton_host = os.getenv("TRYTON_HOST")

config = config.set_xmlrpc("http://admin:%s@%s/tryton/" % (password, tryton_host))

Party = Model.get('party.party')
p1, = Party.find([('id', '=', '1')])
print(p1.name)

Tax = Model.get('account.tax')
ProductTemplate = Model.get('product.template')
Product = Model.get('product.product')
ProductCategoryAccount = Model.get('product.category.account')
Currency = Model.get('currency.currency')
Invoice = Model.get('account.invoice')
InvoiceLine = Model.get('account.invoice.line')

cols = []
vendors = {}

# Load the list of vendors

with open("vendors.csv", "rt") as f:
    csv_reader = csv.reader(f, delimiter=",")
    for row in csv_reader:
        if len(cols) == 0:
            cols = row
        else:
            k = row[0]
            vendors[k] = {}
            i = 1
            for v in row[1:]:
                vendors[k][cols[i]] = v
                i = i + 1

# For each vendor, obtain the relevant objects from Tryton

for vname in vendors.keys():
    vendor = vendors[vname]
    p, = Party.find(['name','=',vendor['name']])
    vendor['_party'] = p 
    t, = Tax.find(['name','=',vendor['tax']])
    vendor['_tax'] = t.id
    product_tmpl, = ProductTemplate.find(['name','=',vendor['product']])
    vendor['_product_template'] = product_tmpl
    product, = Product.find(['template','=',product_tmpl.id])
    vendor['_product'] = product
    vendor['_account'] = product_tmpl.account_category.account_expense
    
# Load the JSON invoices from the file

invcs = []
        
with open("invoices.json", "rt") as f:
    for l in f:
        # if the file was created by Python, fix the quoting style
        l = l.replace("'", '"')
        invc_params = json.loads(l)
        invcs.append(invc_params)

print("Loaded %d invoices" % (len(invcs)))

# Load each invoice into Tryton through the API

for invc_params in invcs:
    vendor = vendors[invc_params['vendor']]
    print(invc_params)
    print(vendor)
    ref = invc_params['invoice_number']
    # Check if the invoice was already loaded
    invoices = Invoice.find(['reference','=',ref])
    if len(invoices) > 0:
        print("Invoice %s already exists" % (ref))
        continue
    print("Adding invoice %s ..." % (ref))
    invoice = Invoice()
    invoice.type = 'in'
    invoice.party = vendor['_party']
    invoice.reference = ref
    invoice.invoice_date = datetime.date.fromisoformat(invc_params['date'])
    invoice.currency, = Currency.find(['code','=',invc_params['currency']])
    line0 = InvoiceLine()
    line0.product = vendor['_product']
    line0.account = vendor['_account']
    line0.quantity = 1
    line0.unit_price = get_price(invc_params['amount_ex'])
    # newly created InvoiceLine has default tax code for the product.
    # we delete the default tax code:
    line0.taxes.clear()
    # and insert the tax code that was specified for the vendor:
    line0.taxes.append(Tax(vendor['_tax']))
    invoice.lines.append(line0)
    invoice.tax_amount = get_price(invc_params['tax'])
    invoice.total_amount = get_price(invc_params['amount'])
    invoice.save()

